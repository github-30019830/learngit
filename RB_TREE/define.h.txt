#ifndef DEFINE_H
#define DEFINE_H

#define MAX_NUMBER 5000
#define ELEMENTS_NUM 15
#define BLACK 1
#define RED   0
#define NODE struct node


struct node {
	int key;
	int color;
	bool NIL;
	NODE *parent;
	NODE *left;
	NODE *right;
};

NODE* Creat_RB_TREE(void);
NODE *INSERT(NODE* head, int key);
NODE *RB_DELETE( NODE* head,int key);
NODE *left_rotate(NODE*head, NODE* x);
NODE *right_rotate(NODE*head, NODE* x);
NODE *RB_INSERT_FIXUP(NODE *head,NODE* x);
NODE* PREDECESSOR(NODE *head, NODE* x);
NODE* SUCCESSOR(NODE *head, NODE* x);
NODE *MINIMUM(NODE *head);
NODE *MAXMUM(NODE *head);
NODE *RB_TRANSPLANT(NODE* head, NODE* x, NODE *y);
NODE* SEARCH_NODE(NODE* head, int key);
NODE* RB_DELETE_FIXUP(NODE* head, NODE* x);
void Preoder_Traverse(NODE* head);
void DRAW_TREE(NODE* head);
void QUICKSORT(int *a,int n);

#endif
